#include <stdlib.h>
#include "bitStream.h"

#define ERROR -1

int writeBitStream(BitStreamWriter* bs);

BitStreamWriter* bsOpen(FILE* fp) {
	if (fp == NULL) {
		return NULL;
	}

	BitStreamWriter* bs = (BitStreamWriter *)malloc(sizeof(BitStreamWriter));
	if (bs == NULL) {
		return NULL;
	}

	bs->fp = fp;
	bs->bitCount = 0;
	bs->buffer = 0;

	return bs;
}

void bsClose(BitStreamWriter* bs) {
	free(bs);
}

int bsFlush(BitStreamWriter* bs) {
	if (bs == NULL) {
		return ERROR;
	}

	if (bs->bitCount > 0) {
		return writeBitStream(bs);
	}
	else {
		return 0;
	}
}

int bsWrite32(uint32_t value, uint8_t bitCount, BitStreamWriter* bs) {
	int result = 0;

	if ((bs == NULL) || (bitCount > 32)) {
		return ERROR;
	}
	else if (bitCount == 0) {
		return 0;
	}

	if ((bitCount + bs->bitCount) <= 32) {
		bs->bitCount += bitCount;
		bs->buffer |= (value & ~(0xFFFFFFFF << bitCount)) << (32 - bs->bitCount);
		if (bs->bitCount == 32) {
			result = writeBitStream(bs);
		}
	}
	else {
		uint8_t oldBc = bs->bitCount;
		uint32_t oldBuffer = bs->buffer;
		bs->bitCount = 32;
		bs->buffer |= (value & ~(0xFFFFFFFF << bitCount)) >> (oldBc + bitCount - 32);
		result = writeBitStream(bs);
		if (result == ERROR) {
			//Resort old values, since write failed.
			bs->bitCount = oldBc;
			bs->buffer = oldBuffer;
		}
		else {
			bs->bitCount = oldBc + bitCount - 32;
			bs->buffer = (value & ~(0xFFFFFFFF << bs->bitCount)) << (32 - bs->bitCount);
		}
	}

	return result;
}

int writeBitStream(BitStreamWriter* bs) {
	uint8_t i, byteCount;

	if (bs->bitCount == 0) {
		return 0;
	}

	byteCount = ((bs->bitCount - 1) / 8) + 1;
	
	for (i = 4; i > (4 - byteCount); i--) {
		//Bytes in buffer are left aligned.
		uint8_t shift = 8 * (i - 1);
		uint8_t output = (bs->buffer & (0xFF << shift)) >> shift;
		fputc(output, bs->fp);
		if (ferror(bs->fp)) {
			return ERROR;
		}
	}

	uint8_t bc = bs->bitCount;
	bs->bitCount = 0;
	bs->buffer = 0;
	return bc;
}
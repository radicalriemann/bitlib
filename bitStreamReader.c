#include "bitStream.h"
#include <stdio.h>

uint32_t bs_refreshBuf(BitStreamReader *bsReader);

void BS_init(BitStreamReader *bsReader, char *filePath, uint32_t bufferSize)
{
    bsReader->file = fopen(filePath, "rb");

    bsReader->buffer = (uint8_t *)malloc(sizeof(uint8_t) * bufferSize);

    bsReader->bufferSize = bufferSize;

    size_t readCount = fread(bsReader->buffer, sizeof(uint8_t), bufferSize, bsReader->file);

    bsReader->count = readCount;

    bsReader->readHead = 0;
    bsReader->readHeadIndex = 0;
}

void BS_close(BitStreamReader *bsReader)
{
    free(bsReader->buffer);
    fclose(bsReader->file);
}

uint32_t BS_readword(BitStreamReader *bsReader, uint32_t wordLength)
{
    // Assume this is the first read and that we're currently byte aligned
    uint32_t buf = 0x00000000;
    uint32_t i;
    for(i = 0; i < wordLength; i++)
    {
        buf |= (bsReader->buffer[i]) << ((wordLength - i - 1) * 8);
    }
    bsReader->readHeadIndex += 4;
    return buf;
}

bool BS_readbit(BitStreamReader *bsReader, bool *dst)
{
    if(bsReader->readHeadIndex >= bsReader->count)
    {
        //try to refresh the buffer
        uint32_t readCount = bs_refreshBuf(bsReader);
        bsReader->count = readCount;
        
        bsReader->readHeadIndex = 0;
        bsReader->readHead = 0;

        if(readCount == 0)
        {
            return false;
        }
    }
    uint8_t val = bsReader->buffer[bsReader->readHeadIndex];
    bool bit = select_bit(val, bsReader->readHead);
    bsReader->readHead++;

    if(bsReader->readHead == sizeof(uint8_t) * 8)
    {
        bsReader->readHead = 0;
        bsReader->readHeadIndex++;
    }

    *dst = bit;
    return true;
}

uint32_t bs_refreshBuf(BitStreamReader *bsReader)
{
    size_t readCount = fread(bsReader->buffer, sizeof(uint8_t), bsReader->bufferSize, bsReader->file);
    bsReader->count = readCount;
    return( readCount );
}
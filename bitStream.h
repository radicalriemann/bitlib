#ifndef BITSTREAM_H
#define BITSTREAM_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define select_bit(val, pos) ((val & (1 << (8 - pos - 1))) >> (8 - pos - 1))

typedef struct {
	uint32_t buffer;
	uint8_t bitCount;
	FILE* fp;
}BitStreamWriter;

typedef struct {
    uint8_t *buffer;
    int32_t count;
    int32_t bufferSize;
    uint8_t readHead;
    int32_t readHeadIndex;
    FILE *file;
}BitStreamReader;

BitStreamWriter* bsOpen(FILE* fp);

void bsClose(BitStreamWriter* bs);
int bsFlush(BitStreamWriter* bs);
int bsWrite32(uint32_t value, uint8_t bitCount, BitStreamWriter* bs);

void BS_init(BitStreamReader *bsReader, char *filePath, uint32_t bufferSize);
bool BS_readbit(BitStreamReader *bsReader, bool *dst);
uint32_t BS_readword(BitStreamReader *bsReader, uint32_t wordLength);
void BS_close(BitStreamReader *bsReader);

#endif // !BITSTREAM_H
